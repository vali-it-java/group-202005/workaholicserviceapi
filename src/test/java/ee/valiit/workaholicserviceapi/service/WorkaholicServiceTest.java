package ee.valiit.workaholicserviceapi.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class WorkaholicServiceTest {

    WorkaholicService workaholicService;

    @BeforeEach
    public void setUp() {
        workaholicService = new WorkaholicService();
    }

    @Test
    public void testIsItWeekend() {

        boolean isItWeekend = workaholicService.isItWeekend("2020-06-04 09:27");
        Assertions.assertFalse(isItWeekend, "Incorrect response!");

        isItWeekend = workaholicService.isItWeekend("2020-06-05 14:12");
        Assertions.assertFalse(isItWeekend, "Incorrect response!");

        isItWeekend = workaholicService.isItWeekend("2020-06-05 16:00");
        Assertions.assertTrue(isItWeekend, "Incorrect response!");

        isItWeekend = workaholicService.isItWeekend("2020-06-06 15:23");
        Assertions.assertTrue(isItWeekend, "Incorrect response!");

        isItWeekend = workaholicService.isItWeekend("2020-06-07 21:56");
        Assertions.assertTrue(isItWeekend, "Incorrect response!");

        isItWeekend = workaholicService.isItWeekend("2020-06-08 05:59");
        Assertions.assertTrue(isItWeekend, "Incorrect response!");

        isItWeekend = workaholicService.isItWeekend("2020-06-08 06:00");
        Assertions.assertFalse(isItWeekend, "Incorrect response!");

        isItWeekend = workaholicService.isItWeekend("2020-06-10 16:53");
        Assertions.assertFalse(isItWeekend, "Incorrect response!");
    }
}
