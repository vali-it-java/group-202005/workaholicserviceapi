package ee.valiit.workaholicserviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkaholicserviceapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkaholicserviceapiApplication.class, args);
	}

}
