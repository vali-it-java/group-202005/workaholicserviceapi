package ee.valiit.workaholicserviceapi.service;

import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class WorkaholicService {

    public boolean isItWeekend(String dateString) { // "2020-06-04 09:18" --> false
        // 2020-06-04 09:18" --> LocalDateTime
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(dateString, formatter);

        if (dateTime.getDayOfWeek() == DayOfWeek.SATURDAY || dateTime.getDayOfWeek() == DayOfWeek.SUNDAY) {
            return true;
        } else if(dateTime.getDayOfWeek() == DayOfWeek.MONDAY) {
            if (dateTime.getHour() < 6) {
                return true;
            } else {
                return false;
            }
        } else if (dateTime.getDayOfWeek() == DayOfWeek.FRIDAY) {
            if (dateTime.getHour() >= 16) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }
}
