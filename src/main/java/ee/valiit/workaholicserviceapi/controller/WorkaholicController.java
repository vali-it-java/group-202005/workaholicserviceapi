package ee.valiit.workaholicserviceapi.controller;

import ee.valiit.workaholicserviceapi.service.WorkaholicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("weekenddetector")
public class WorkaholicController {

    @Autowired
    private WorkaholicService workaholicService;

    @PostMapping("/test")
    public boolean isItWeekend(@RequestParam String dateString) {
        return workaholicService.isItWeekend(dateString);
    }
}
